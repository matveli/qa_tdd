﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_TDD
{
    public class Friend
    {
        public string name;
        public DateTime Birthday;

        public Friend() { }

        public Friend(string _name, DateTime _bd)
        {
            name = _name;
            Birthday = _bd;
        }
    }

    public class BDNotifier
    {
        public List<Friend> friendsBook;

        public BDNotifier()
        {
            friendsBook = new List<Friend>();

            friendsBook.Add(new Friend("Eva", new DateTime(1994, 12, 04)));
            friendsBook.Add(new Friend("Diana", new DateTime(1994, 12, 04)));
            friendsBook.Add(new Friend("Adam", new DateTime(2001, 12, 04)));
        }

        public int CntBD(DateTime date)
        {
            int cnt = 0;

            foreach (var friend in friendsBook)
            {
                if (friend.Birthday == date)
                    cnt++;
            }

            return cnt;
        }

        public DateTime BDOfFriend(string name)
        {
            foreach (var friend in friendsBook)
            {
                if (name == friend.name)
                {
                    return friend.Birthday;
                }
            }
            return new DateTime();
        }

        public List<string> BDOnDate(DateTime date)
        {
            List<string> result = new List<string>();

            foreach (var friend in friendsBook)
            {
                if (friend.Birthday.Date == date)
                {
                    result.Add(friend.name);
                }
            }

            return result;
        }
    }
}
