﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA_TDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_TDD.Tests
{
    [TestClass()]
    public class BDNotifierTests
    {
        [TestMethod()]
        public void isClassBDNExists()
        {
            var bn = new BDNotifier();

            //assert
            Assert.AreNotEqual(bn, null);
        }

        [TestMethod()]
        public void isFriendsBookExists()
        {
            var bn = new BDNotifier();

            //assert
            Assert.AreNotEqual(bn.friendsBook, null);
        }

        [TestMethod()]
        public void CntBDTest()
        {
            //arrange
            DateTime date = new DateTime(1994, 12, 04);
            int expected = 2;

            //actual
            BDNotifier bn = new BDNotifier();
            int actual = bn.CntBD(date);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void BDOfFriendTest()
        {
            //arrange
            string name = "Adam";
            DateTime expected = new DateTime(2001, 12, 04);

            //actual
            BDNotifier bn = new BDNotifier();
            DateTime actual = bn.BDOfFriend(name);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void BDOfFriendTest2()
        {
            //arrange
            string name = "Eva";
            DateTime expected = new DateTime(1994, 12, 04);

            //actual
            BDNotifier bn = new BDNotifier();
            DateTime actual = bn.BDOfFriend(name);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void BDOnDateTest()
        {
            //arrange
            DateTime date = new DateTime(1994, 12, 04);
            var expected = new List<string>() {"Eva", "Diana"};

            //actual
            BDNotifier bn = new BDNotifier();
            var actual = bn.BDOnDate(date);

            //assert
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}