﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA_TDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_TDD.Tests
{
    [TestClass()]
    public class FriendTests
    {
        [TestMethod()]
        public void isCorrectConstructorFriendTest()
        {
            Friend exFriend = new Friend();
            exFriend.name = "Adam";
            exFriend.Birthday = new DateTime(2009, 12, 12);

            Friend actFriend = new Friend("Adam", new DateTime(2009, 12, 12));

            if(exFriend.name != actFriend.name || exFriend.Birthday != actFriend.Birthday)
                Assert.Fail();
        }

        [TestMethod()]
        public void isClassFriendExists()
        {
            var fr = new Friend();

            //assert
            Assert.AreNotEqual(fr, null);
        }

    }

}